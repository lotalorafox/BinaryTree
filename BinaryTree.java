public class BinaryTree{

    private int key;
    private BinaryTree parent;
    private BinaryTree left;
    private BinaryTree right;

    public BinaryTree(int key) {
        super();
        this.key = key;
        this.left = null;
        this.right = null;
    }

    public BinaryTree(int key, BinaryTree left, BinaryTree right) {
        super();
        this.key = key;
        setLeft(left);
        setRight(right);
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public BinaryTree getParent() {
        return parent;
    }

    public void setParent(BinaryTree parent) {
        this.parent = parent;
    }

    public BinaryTree getLeft() {
        return left;
    }

    public void setLeft(BinaryTree left) {
        this.left = left;
        if (left != null) {
            left.setParent(this);
        }
    }

    public BinaryTree getRight() {
        return right;
    }

    public void setRight(BinaryTree right) {
        this.right = right;
        if (right != null) {
            right.setParent(this);
        }
    }

    public int height() {
        int l = 0;
        if (this.left != null) {
            l = left.height();
        }
        int r = 0;
        if (this.right != null) {
            r = right.height();
        }
        return 1 + Math.max(l, r);

    }

    public void printInOrder() {
        printInOrder(this);
        System.out.println();
    }

    public void printInOrder(BinaryTree tree) {
        if (tree != null) {
            printInOrder(tree.getLeft());
            System.out.print(tree.getKey() + " ");
            printInOrder(tree.getRight());
        }
    }

    public void printPreOrder() {
        printPreOrder(this);
        System.out.println();
    }

    public void printPreOrder(BinaryTree tree) {
        if (tree != null) {
            System.out.print(tree.getKey() + " ");
            printPreOrder(tree.getLeft());
            printPreOrder(tree.getRight());
        }
    }

    public void printPostOrder() {
        printPostOrder(this);
        System.out.println();
    }

    public void printPostOrder(BinaryTree tree) {
        if (tree != null) {
            printPostOrder(tree.getLeft());
            printPostOrder(tree.getRight());
            System.out.print(tree.getKey() + " ");
        }
    }
    public void insertInt(int val){
            if((int)key > val){
                if(this.right == null){
                    this.right = new BinaryTree(val);
                }else{
                    this.right.insertInt(val);
                }
            }else{
                if(this.left == null){
                    this.left = new BinaryTree(val);
                }else{
                    this.left.insertInt(val);
                }
                
            }
    }
}