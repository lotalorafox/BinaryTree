/**
 * BinaryTreeInt
 * by lotalorafox
 */
public class BinaryTreeInt {
    int val;
    BinaryTreeInt parent;
    BinaryTreeInt left;
    BinaryTreeInt right;

    // -------------------------------constructors------------------------------
    public BinaryTreeInt(){
        this.val = 0;
        this.parent =null;
        this.left = this.right = null;
    }
    public BinaryTreeInt(int v){
        this.val = v;
        this.left = this.right = new BinaryTreeInt();
        setLeft(this.left);
        setRight(this.right);
    }
    public BinaryTreeInt(int v, BinaryTreeInt p){
        this.val = v;
        this.setParent(p);
    }
    //--------------------------------Set methods---------------------------------
    public void setLeft(BinaryTreeInt newLeft)
    {
        if (left != null && left.getParent() == this) left.setParent(null);
        left = newLeft;
        left.setParent(this);
    }
    public void setRight(BinaryTreeInt newRight)
    {
        if (this.right != null && this.right.getParent() == this) this.right.setParent(null);
        this.right = newRight;
        this.right.setParent(this);
    }
    public void setParent(BinaryTreeInt newParent){
        this.parent = newParent;
    }
    public void setValue(int value)
    {
        val = value;
    }
    //---------------------------------------------Get methods-----------------------------------
    public BinaryTreeInt getLeft()
    {
        return left;
    }
    public BinaryTreeInt getRight(){
        return right;
    }
    public int getValue(){
        return this.val;
    }
    public BinaryTreeInt getParent(){
        return this.parent;
    }
    //---------------------------------------------Get info-------------------------------------
    // public boolean haveChildsAvaible(){
    //     if(left.val == 0 && right.val == 0){
    //         return true;
    //     }
    //     return false;
    // }
    // --------------------------------------------Methods ------------------------------------
    public void insert(int value){
         if(this.val == 0){
            this.val = value;
         }else{
             if(value > this.val){
                 if(this.right == null){
                     this.right = new BinaryTreeInt();
                 }
                 this.right.insert(value);
             }else{
                if(this.left == null){
                    this.left = new BinaryTreeInt();
                }
                 this.left.insert(value);
             }
         }
    }
    public void print(){
        if(this.right == null && this.left == null){
            System.out.print(this.val + " ");
        }else{
            if(this.right != null){}
                this.right.print();
            }
            if(this.left != null){
                this.left.print();
            }
            this.parent.print();
        }
}
 